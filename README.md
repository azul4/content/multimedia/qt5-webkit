# qt5-webkit

Required by xnviewmp. Classes for a WebKit2 based implementation and a new QML API.

https://github.com/qtwebkit/qtwebkit

<br><br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/multimedia/qt5-webkit.git
```

